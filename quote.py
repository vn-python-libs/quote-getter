"""
GetQuote is simple quote getter from https://citaitons-celebres.fr/

Author Didier Bröska <didier.broska@gmail.com>
"""
import requests
from html.parser import HTMLParser
import json

__version__="0.2"


class GetQuote(HTMLParser):
    """Class to get a quote from citations-celebre.fr"""

    def __init__(self, url):
        """
        Constructor call HTMLParser constructor and get html file form url
        argument.
        Request test if status is OK.
        If ok run feed method or raise ConnectionError exception
        """
        super().__init__()
        html = requests.get(url)
        if html.ok:
            self.feed(html.text)
        else:
            raise ConnectionError

    def handle_data(self, data):
        """
        Handler html data and test if quote section is found.
        Quote and originator are stored in quote and originator attributes.
        """
        if data.find("quotes") > -1:
            citation = json.loads(data)
            self.quote = citation['quote'][1:-1]
            self.originator = citation['originator']

    def getQuote(self):
        """
        Getter return string with quote
        """
        return self.quote

    def getOriginator(self):
        """
        Getter return string with Originator name
        """
        return self.originator
